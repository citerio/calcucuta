package citerio.app.calcucuta

import android.annotation.TargetApi
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    // Represent whether the lastly pressed key is numeric or not
    var lastNumeric: Boolean = false

    // Represent that current state is in error or not
    var stateError: Boolean = false

    // If true, do not allow to add another DOT
    var lastDot: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()


    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    fun init(){
        ppd.showSoftInputOnFocus = false
        bpd.showSoftInputOnFocus = false
        ppp.showSoftInputOnFocus = false
        upp.showSoftInputOnFocus = false

        ppd.addTextChangedListener(MyTextWatcher(ppd))
        bpd.addTextChangedListener(MyTextWatcher(bpd))
        ppp.addTextChangedListener(MyTextWatcher(ppp))
        upp.addTextChangedListener(MyTextWatcher(upp))
        ppd.setText(BaseApplication.Settings.pesosPorDolar)
        bpd.setText(BaseApplication.Settings.bolivaresPorDolar)
        upp.setText("1")

    }

    /*****************************validating data entries*****************************/
    private inner class MyTextWatcher(private val view: View) : TextWatcher {

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

        }

        override fun afterTextChanged(s: Editable) {
            when (view.id) {
                R.id.ppd -> {
                    validate_bpp_ppb(s.toString())
                    validate_ced_ceb(s.toString())
                }
                R.id.bpd -> validate_bpp_ppb(s.toString())
                R.id.ppp -> validate_ced_ceb(s.toString())
                R.id.upp -> validate_cud_cub(s.toString())
            }

        }
    }

    private fun validate_bpp_ppb(text: String): Boolean {

        if (ppd.text.toString().isNotEmpty() && bpd.text.toString().isNotEmpty()) {
            BaseApplication.Settings.pesosPorDolar = ppd.text.toString()
            BaseApplication.Settings.bolivaresPorDolar = bpd.text.toString()
            onBolivaresPorPesos()
            onPesosPorBolivares()
        }

        return true

    }

    private fun validate_ced_ceb(text: String): Boolean {

        if (ppp.text.toString().isNotEmpty() && ppd.text.toString().isNotEmpty() && ppb.text.toString().isNotEmpty()) {
            onCostoEnDolares()
            onCostoEnBolivares()
        }

        return true

    }

    private fun validate_cud_cub(text: String): Boolean {

        if (ced.text.toString().isNotEmpty() && ceb.text.toString().isNotEmpty() && upp.text.toString().isNotEmpty()) {
            onCostoUndDolares()
            onCostoUndBolivares()
        }

        return true

    }

    /**
     * Append the Button.text to the TextView
     */
    fun onDigit(view: View) {
        if (stateError) {
            // If current state is Error, replace the error message
            //txtInput.text = (view as Button).text
            stateError = false
        } else {
            // If not, already there is a valid expression so append to it
            if (ppd.hasFocus()) {
                ppd.append((view as Button).text)
            } else if (bpd.hasFocus()) {
                bpd.append((view as Button).text)
            } else if (ppp.hasFocus()) {
                ppp.append((view as Button).text)
            } else if (upp.hasFocus()) {
                upp.append((view as Button).text)
            }
        }
        // Set the flag
        lastNumeric = true
    }

    /**
     * Append . to the TextView
     */
    fun onDecimalPoint(view: View) {
        /*if (lastNumeric && !stateError && !lastDot) {
            txtInput.append(".")
            lastNumeric = false
            lastDot = true
        }*/
        if (ppd.hasFocus() && !ppd.text.contains(".")) {
            ppd.append(".")
        } else if (bpd.hasFocus() && !bpd.text.contains(".")) {
            bpd.append(".")
        } else if (ppp.hasFocus() && !ppp.text.contains(".")) {
            ppp.append(".")
        }
    }

    /**
     * Append +,-,*,/ operators to the TextView
     */
    /*fun onOperator(view: View) {
        if (lastNumeric && !stateError) {
            txtInput.append((view as Button).text)
            lastNumeric = false
            lastDot = false    // Reset the DOT flag
        }
    }*/


    /**
     * Clear the TextView
     */
    fun onClear(view: View) {
        /*this.txtInput.text = ""
        lastNumeric = false
        stateError = false
        lastDot = false*/
    }

    fun onDelete(view: View) {
        if (ppd.hasFocus()) {
            ppd.setText(ppd.text.dropLast(1))
            ppd.placeCursorToEnd()
        } else if (bpd.hasFocus()) {
            bpd.setText(bpd.text.dropLast(1))
            bpd.placeCursorToEnd()
        } else if (ppp.hasFocus()) {
            ppp.setText(ppp.text.dropLast(1))
            ppp.placeCursorToEnd()
        } else if (upp.hasFocus()) {
            upp.setText(upp.text.dropLast(1))
            upp.placeCursorToEnd()
        }
    }

    fun onBolivaresPorPesos() {

        bpp.text =
            (bpd.text.toString().toDouble().div(ppd.text.toString().toDouble())).round(2)
                .toString()
    }

    fun onPesosPorBolivares() {

        ppb.text =
            (ppd.text.toString().toDouble().div(bpd.text.toString().toDouble())).round(2)
                .toString()
    }

    fun onCostoEnDolares() {
        ced.text =
            (ppp.text.toString().toDouble().div(ppd.text.toString().toDouble())).round(2)
                .toString()
        onCostoUndDolares()
    }

    fun onCostoEnBolivares() {
        ceb.text =
            (ppp.text.toString().toDouble().div(ppb.text.toString().toDouble())).round(2)
                .toString()
        onCostoUndBolivares()
    }

    fun onCostoUndDolares() {
        cud.text =
            (ced.text.toString().toDouble().div(upp.text.toString().toDouble())).round(2)
                .toString()
    }

    fun onCostoUndBolivares() {
        cub.text =
            (ceb.text.toString().toDouble().div(upp.text.toString().toDouble())).round(2)
                .toString()
    }

    fun Double.round(decimals: Int = 2): Double = "%.${decimals}f".format(this).toDouble()

    fun EditText.placeCursorToEnd() {
        this.setSelection(this.text.length)
    }
}
