package citerio.app.calcucuta

import android.app.Application
import android.content.Context




class BaseApplication : Application() {
    private val TAG = "AEP log"


    companion object{
        lateinit var context: Context;

        fun getAppContext(): Context? {
            return context
        }

        fun get(): BaseApplication {
            return BaseApplication.getAppContext()?.applicationContext as BaseApplication
        }
    }


    override fun onCreate() {
        super.onCreate()


        //The following will disable bugsnag
        //Bugsnag.disableExceptionHandler()

        context = applicationContext



        Init()


    }

    fun Init(){


    }

     object Settings {


        object Preferences {
            const val pesosPorDolar = "50716bc4-2e15-4b70-a4f7-a8b8217fe753"
            const val bolivaresPorDolar = "a2d70c15-6ded-41cb-8004-6ddafc78383e"


            /*
            1d63617e-596b-4f45-a4c9-a2d628719429
            2e2e4184-86f7-4376-a1eb-fe6953668d59
            ceb6583e-3fc4-426c-a971-e89c0a7777ed
            612876aa-3759-4a95-98f9-faf31df0ecbb
            51fb9d01-48f6-4254-b2f3-9e9d71cf5f3a
            1f6b7f53-bcc9-469d-aff3-c7661264a5e2
            c6ac82ac-86a3-4bff-acf4-0b374b2e87bf
            dc71049f-79ba-41f8-9032-fcc8219f54cc
            002acb71-1ede-4b8c-bd29-464f990c940d
            */
        }

        var pesosPorDolar : String
            get() = citerio.app.calcucuta.Common.Preferences.get(Settings.Preferences.pesosPorDolar, "", BaseApplication.context)
            set(value) {
                citerio.app.calcucuta.Common.Preferences.save(Settings.Preferences.pesosPorDolar, value, BaseApplication.context)
            }

         var bolivaresPorDolar : String
             get() = citerio.app.calcucuta.Common.Preferences.get(Settings.Preferences.bolivaresPorDolar, "", BaseApplication.context)
             set(value) {
                 citerio.app.calcucuta.Common.Preferences.save(Settings.Preferences.bolivaresPorDolar, value, BaseApplication.context)
             }

    }

    object Utilities {

    }
}