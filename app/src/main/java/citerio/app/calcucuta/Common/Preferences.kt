package citerio.app.calcucuta.Common

import android.content.Context

object Preferences {
    fun delete(key: String, context: Context): Boolean {
        val preferences = context.getSharedPreferences("PersistentStore", Context.MODE_PRIVATE)

        val editor = preferences.edit()

        return editor.remove(key).commit();
    }


    fun save(key: String, default: String, context: Context) : Boolean {
        return save(
            String::class.java,
            key,
            default,
            context
        )
    }

    fun save(key: String, default: Boolean, context: Context) : Boolean {
        return save(
            Boolean::class.java,
            key,
            default,
            context
        )
    }

    fun save(key: String, default: Int, context: Context) : Boolean {
        return save(Int::class.java, key, default, context)
    }

    fun save(key: String, default: Long, context: Context) : Boolean {
        return save(
            Long::class.java,
            key,
            default,
            context
        )
    }

    fun save(key: String, default: Float, context: Context) : Boolean {
        return save(
            Float::class.java,
            key,
            default,
            context
        )
    }

    fun <T> save(clazz: Class<T>, key: String, value: Any, context: Context) : Boolean {
        val preferences = context.getSharedPreferences("PersistentStore", Context.MODE_PRIVATE)
        val editor = preferences.edit()

        when (clazz) {
            Boolean::class.java -> editor.putBoolean(key, value as Boolean)
            String::class.java -> editor.putString(key, value as String)
            Int::class.java -> editor.putInt(key, value as Int)
            Long::class.java -> editor.putLong(key, value as Long)
            Float::class.java -> editor.putFloat(key, value as Float)
        }

        return editor.commit()
    }


    fun get(key: String, default: String, context: Context) : String {
        return get(
            String::class.java,
            key,
            default,
            context
        ) as String
    }

    fun get(key: String, default: Boolean, context: Context) : Boolean {
        return get(
            Boolean::class.java,
            key,
            default,
            context
        ) as Boolean
    }

    fun get(key: String, default: Int, context: Context) : Int {
        return get(
            Int::class.java,
            key,
            default,
            context
        ) as Int
    }

    fun get(key: String, default: Long, context: Context) : Long {
        return get(
            Long::class.java,
            key,
            default,
            context
        ) as Long
    }

    fun get(key: String, default: Float, context: Context) : Float {
        return get(
            Float::class.java,
            key,
            default,
            context
        ) as Float
    }

    fun <T> get(clazz: Class<T>, key: String, default: Any, context: Context) : T? {
        var result: T? = null

        val preferences = context.getSharedPreferences("PersistentStore", Context.MODE_PRIVATE)

        when (clazz) {
            Boolean::class.java -> return preferences.getBoolean(key, default as Boolean) as T
            String::class.java -> return preferences.getString(key, default as String) as T
            Int::class.java -> return preferences.getInt(key, default as Int) as T
            Long::class.java -> return preferences.getLong(key, default as Long) as T
            Float::class.java -> return preferences.getFloat(key, default as Float) as T
        }

        return result;
    }



    fun saveBytes(key: String, bytes: ByteArray, context: Context): Boolean {
        //http://stackoverflow.com/questions/19556433/saving-byte-array-using-sharedpreferences
        val preferences = context.getSharedPreferences("PersistentStore", Context.MODE_PRIVATE)
        val editor = preferences.edit()
        editor.putString(key, String(bytes, Charsets.ISO_8859_1))
        return editor.commit()
    }

    fun getBytes(key: String, context: Context): ByteArray? {
        //http://stackoverflow.com/questions/19556433/saving-byte-array-using-sharedpreferences
        val preferences = context.getSharedPreferences("PersistentStore", Context.MODE_PRIVATE)
        val str = preferences.getString(key, null)
        if (str != null) {
            return str.toByteArray(Charsets.ISO_8859_1)
        }
        return null
    }


    /*
    fun get(key: Enum<*>, context: Context = getActivity()!!): String? {
        return get(key.toString(), null, context)
    }

    fun get(key: Enum<*>, defaultValue: String, context: Context = getActivity()!!): String? {
        return get(key.toString(), defaultValue, context)
    }

    fun get(key: String, context: Context = getActivity()!!): String? {
        return get(key, null, context)
    }

    fun get(key: String, defaultValue: String?, context: Context = getActivity()!!): String? {
        val result: String?
        val preferences = context.getSharedPreferences("PersistentStore", Context.MODE_PRIVATE)
        result = preferences.getString(key, defaultValue)
        return result
    }

    fun save(key: Enum<*>, value: String, context: Context = getActivity()!!): Boolean {
        return save(key.toString(), value, context)
    }

    fun save(key: String, value: String, context: Context = getActivity()!!): Boolean {
        val preferences = context.getSharedPreferences("PersistentStore", Context.MODE_PRIVATE)
        val editor = preferences.edit()
        editor.putString(key, value)
        return editor.commit()
    }
    */
}